#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):

    # Declaramos la lista y las etiquetas
    def __init__(self):

        self.list_tags = []

        self.tags = ["root-layout", "region", "img", "audio", "textstream"]

        self.attrib = {"root-layout": ['width', 'height', 'background-color'],
                       "region": ['id', 'top', 'bottom', 'left', 'right'],
                       "img": ['src', 'region', 'begin', 'dur'],
                       "audio": ['src', 'begin', 'dur'],
                       "textstream": ['src', 'region']}

    # Añadimos los atributos a las etiquetas y metemos en la lista
    def startElement(self, name, attrs):

        if name in self.tags:
            dicc = {}
            dicc['tag'] = name
            for attribute in self.attrib[name]:
                dicc[attribute] = attrs.get(attribute, "")
            self.list_tags.append(dicc)

    # Devuelve una lista con las etiquetas y sus atributos
    def get_tags(self):
        return self.list_tags


if __name__ == "__main__":

    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))
    print(cHandler.get_tags())
