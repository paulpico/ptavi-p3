#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from smallsmilhandler import SmallSMILHandler
from urllib.request import urlretrieve
import sys
import json


class KaraokeLocal(object):

    def __init__(self, file):
        # Parsea el file
        parser = make_parser()
        cHandler = SmallSMILHandler()
        parser.setContentHandler(cHandler)
        parser.parse(open(file))
        self.listado = cHandler.get_tags()

    def __str__(self):
        # Imprime las etiquetas del file y sus atributos si tienen valor
        for tags in self.listado:
            list_attrib = []
            for attribute in tags:
                value = tags[attribute]
                if attribute != 'tag' and value != "":
                    list_attrib += ("\t", attribute, '="', value, '"')
            print(tags['tag'], "".join(list_attrib))

    def to_json(self, file, jfile=""):
        # Transforma el file a .json
        if jfile == "":
            jfile = file.replace(".smil", ".json")
        with open(jfile, "w") as json_file:
            json.dump(self.listado, json_file, indent=4)

    def do_local(self):
        # Se descarga en local los archivos
        for tags in self.listado:
            for attribute in tags:
                if attribute == 'src':
                    url = tags[attribute]
                    if url.startswith('http://'):
                        fichero = url.split('/')[-1]
                        urlretrieve(url, fichero)
                        tags[attribute] = fichero


if __name__ == "__main__":
    try:
        file = KaraokeLocal(sys.argv[1])
    except IndexError:
        sys.exit('Usage: python3 karaoke.py file.smil')

    file.__str__()
    file.to_json(sys.argv[1])
    file.do_local()
    file.to_json(sys.argv[1], "local.json")
    file.__str__()
